/*
code to create react app (command in gitbash/terminal)
  npx create-react-app <appName>
*/
/*
  after installing, remove all files inside the src folder except index.js
  remove the README.md file in the root folder of the app
*/
// set up/import dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.js';

/*
ReactDOM.render()
  responsible for injecting/inserting the whole React.js Project inside the webpage
*/
/*
  depricated version of the render() in react
*/
// React.createElement('h1', null, 'Hello World');

/*
  JSX - JavaScript XML - is an extention of JS that let's us create objects which will then be compiled and added as HTML elements

  With JSX..
    -we are able to create HTML elements using JS
    -we are also able to create JS objects that will then be compiled and added as HTML elements

*/
/*
  npm start - to start/launch the react app
*/
ReactDOM.render(
  <App />
  ,document.getElementById('root')
);
